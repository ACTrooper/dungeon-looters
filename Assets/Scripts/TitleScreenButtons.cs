﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using TMPro;
public class TitleScreenButtons : MonoBehaviour
{
    private GameObject player;
    public GameObject AlmanacUI;
    public GameObject PauseMenu;
    public GameObject TitleScreen;
    public GameObject OptionsMenu;
    public Slider VolumeSlider;
    public GameObject ConformationWindow;
    Resolution[] resolutions;
    public TMP_Dropdown resolutionDropdown;
    public Slider LoadingBar;
    public bool LoadingScene = false;
      // Start is called before the first frame update
    void Start()
    {
      player = GameObject.Find("Player");
      resolutions = Screen.resolutions;
      resolutionDropdown.onValueChanged.AddListener(delegate { Screen.SetResolution(resolutions[resolutionDropdown.value].width, resolutions[resolutionDropdown.value].height, false); });
        for (int i = 0; i < resolutions.Length; i++)
        {
            resolutionDropdown.options[i].text = ResToString(resolutions[i]);
            resolutionDropdown.value = i;
            resolutionDropdown.options.Add(new TMP_Dropdown.OptionData(resolutionDropdown.options[i].text));
        }
    }
    string ResToString(Resolution res)
    {
        return res.width + " x " + res.height;
    }
    public void StartGame()
    {
        Debug.Log("StartGame");
        //LoadingBar.gameObject.SetActive(true);
        //LoadingScene = true;
    }
    public void OpenAlmanac()
    {
      AlmanacUI.SetActive(true);
      PauseMenu.SetActive(false);
    }
    public void PauseGame()
    {

    }
    public void QuitGame()
    {
        ConformationWindow.SetActive(true);
    }
    public void ConformQuitGame()
    {
        SceneManager.LoadScene(0);
    }
    public void CancelQuit()
    {
        ConformationWindow.SetActive(false);
    }
    public void Options()
    {
      LeanTween.scale(TitleScreen, Vector3.zero, 1f).setEase(LeanTweenType.easeInBack).setOnComplete(LoadOptions);
    }
    void LoadOptions()
    {
      LeanTween.scale(OptionsMenu, new Vector2(1.491828f, 1.491846f), 1f).setEase(LeanTweenType.easeOutBack);
    }
    public void Titlescreen()
    {
      LeanTween.scale(OptionsMenu, Vector3.zero, 1f).setEase(LeanTweenType.easeInBack).setOnComplete(LoadTitleScreen);
    }
    void LoadTitleScreen()
    {
      LeanTween.scale(TitleScreen, new Vector2(1.491828f, 1.491846f), 1f).setEase(LeanTweenType.easeOutBack);
    }
    // Update is called once per frame
    void Update()
    {
        if(!LoadingScene)
        { return; }
        AsyncOperation ao = SceneManager.LoadSceneAsync(1);
        ao.allowSceneActivation = false;
        while (!ao.isDone)
        {
            // [0, 0.9] > [0, 1]
            float progress = Mathf.Clamp01(ao.progress / 0.9f);
            LoadingBar.value = progress;
            // Loading completed
            if (ao.progress == 0.9f)
            {
                ao.allowSceneActivation = true;
            }
        }
    }
}
