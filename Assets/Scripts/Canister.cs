﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Canister : MonoBehaviour
{
  public GameObject[] AvailiblePowerups;
  public Transform Can;
  public Animation Destuction;
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }
    void OnTriggerEnter2D(Collider2D other)
    {
      if(other.CompareTag("Player"))
      { Break(); }
    }
    public void Break()
    {
      int PowerUpSelecter = Random.Range(0, AvailiblePowerups.GetLength(0));
      Instantiate(AvailiblePowerups[PowerUpSelecter], Can.position, Can.rotation);
      Destuction.Play();
      Destroy(this.gameObject, 25);
    }
}
