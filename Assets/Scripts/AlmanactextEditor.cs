﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AlmanactextEditor : MonoBehaviour
{
  public Text[] BasicSlimeTabText;
  public Text[] FireSlimeTabText;
  public RawImage BasicSlimeImage;
    public Enemies[] Data;
    public GameObject AlmanacUI;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        BasicSlimeTabText[0].text = Data[3].EnemyName;
        BasicSlimeTabText[1].text = Data[3].Description;
        BasicSlimeTabText[2].text = Data[3].DamagePerHit.ToString();
        BasicSlimeTabText[3].text = Data[3].MovementSpeed.ToString();
        BasicSlimeTabText[4].text = Data[3].ViewRange.ToString();
    }
    public void AlmanacCloser()
    {
      AlmanacUI.SetActive(false);
    }
}
