﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PowerupMovementController : MonoBehaviour
{
    public Rigidbody2D rb;
    public float Speed;
    public GameObject Player;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    void OnTriggerStay2D(Collider2D other) 
    {
        if(other.gameObject.CompareTag("Player"))
        {
            this.rb.AddForce((other.transform.position - this.transform.position).normalized * Speed);
        }
    }
}
