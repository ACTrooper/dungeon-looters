﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Player_Controller : MonoBehaviour
{
    // Input
    [Header("Input")]
    [SerializeField]
    private Vector3 leftJoystickInput; // holds the input of the Left Joystick
    [SerializeField]
    private Vector3 rightJoystickInput; // hold the input of the Right Joystick
    public LeftJoystick leftJoystick; // the game object containing the LeftJoystick script
    public RightJoystick rightJoystick; // the game object containing the RightJoystick script
    public Touch touch;
    [Header("Assignables")]
    [Space(10)]
    public GameObject Player; // Player Position
    public Transform LookObjectPosition;
    public Rigidbody2D PlayerRB;
    public Slider HealthSlider;
    public Slider ManaSlider;
    public Slider EXPSlider;
    public Text HealthTxt;
    public Text ManaText;
    public Text EXPText;
    public Text WoodTxt;
    public Text MetalTxt;
    public Text GoldTxt;
    [Header("Movement")]
    [Space(10)]
    public float MovementSpeed;
    public float rotationSpeed;
    public float sensitivity = 1;
    public int TouchArray;
    public bool AbleToMove;
    [Header("Shooting")]
    [Space(10)]
    public GameObject[] ProjectileTypes;
    public ScriptableObject[] WeaponTypes;
    public bool isShooting;
    public Transform Firepoint;
    public int CurrentWeapon;
    [Header("Misc.")]
    [Space(10)]
    public string[] DebugMessages;
    public GameObject DeathMessage;
    public bool Dead;
    public GameObject PauseMenu;
    public GameObject MainUI;
    [Header("Items")]
    [Space(10)]
    public int MetalCount;
    public int WoodCount;
    public int GoldCount;
    public Crafting Crafter;
    [Header("Player Stats")]
    [Space(10)]
    public int health;
    public int MaxHealth;
    public float exp;
    public float mana;
    public float Maxmana;
    public bool Invincible;
    public float InvincabilityTime;





    // Start is called before the first frame update
    void Start()
    {
        Dead = false;
    }
    public void KillPlayer(string Killer)
    {
        MovementSpeed = 0;
        Dead = true;
        Debug.Log(DebugMessages[2] + " By " + Killer);
    }
    void Awake()
    {
        if(PlayerRB == null)
        {
            Debug.LogError(DebugMessages[0]);
        }
        if(Player == null)
        {
            Debug.LogError(DebugMessages[4]);
        }
        if(leftJoystick == null)
        {
            Debug.LogError(DebugMessages[2]);
        }
        if(rightJoystick == null)
        {
            Debug.LogError(DebugMessages[3]);
        }
        if(LookObjectPosition == null)
        {
            Debug.LogError(DebugMessages[1]);
        }

    }
    void FixedUpdate()
    {
        if(health >= MaxHealth)
        {health = MaxHealth; }
        if(mana >= Maxmana)
        {mana = Maxmana;}
        HealthSlider.maxValue = MaxHealth;
        ManaSlider.maxValue = Maxmana;
        string ManaDisplay = mana + "/" + Maxmana;
        string HealthDisplay = health + "/" + MaxHealth;
        HealthTxt.text = HealthDisplay;
        ManaText.text = ManaDisplay;
        HealthSlider.value = health;
        ManaSlider.value = mana;
        EXPSlider.value = exp;
        WoodTxt.text = WoodCount.ToString();
        MetalTxt.text = MetalCount.ToString();
        GoldTxt.text = GoldCount.ToString();
        if (!AbleToMove)
        { return;}
        //Get Input
        InputManager();

        //Look Around
        Look();
    }
    public void PauseGame()
    {
      MainUI.SetActive(false);
      PauseMenu.SetActive(true);
      AbleToMove = false;
    }
    public void UnpauseGame()
    {
      MainUI.SetActive(true);
      PauseMenu.SetActive(false);
      AbleToMove = true;
    }
    // Update is called once per frame
    void Update()
    {
      if (!AbleToMove)
      { return;}
        //Move the player
        MovePlayer();

    if(!isShooting)
    {   return;}
        //Shoot
        Shoot();
    }
    void Shoot()
    {
        Debug.Log(DebugMessages[5]);
        if(WeaponTypes[0].name == "Pistol")
        {
            Instantiate(ProjectileTypes[0], Firepoint.position, Firepoint.rotation);
        }
    }
    void OnCollisionEnter(Collision other)
    {
        if(other.gameObject.CompareTag("SolidWall"))
        {
            Debug.Log(DebugMessages[7]);
            PlayerRB.velocity = Vector3.zero;
        }
    }
    void Look()
    {
        if(leftJoystickInput.x == 0)
        {
            TouchArray = 0;
            return;
        }
        else
        {
            TouchArray = 1;
        }
        if(leftJoystickInput.y == 0)
        {
            TouchArray = 0;
            return;
        }
        else
        {
            TouchArray = 1;
        }
        touch = Input.GetTouch(TouchArray);
        switch (touch.phase)
            {
                // Record initial touch position.
                case TouchPhase.Began:

                    break;
                case TouchPhase.Moved:
                    if (Input.touchCount > TouchArray)
                    {
                    isShooting = true;
                    Vector2 touchPosition = Camera.main.ScreenToWorldPoint(touch.position);
                    LookObjectPosition.position = touchPosition;
                    Player.transform.right = LookObjectPosition.position - Player.transform.position;
                    }
                    break;
                case TouchPhase.Ended:
                    isShooting = false;
                    break;
            }
    }
    void MovePlayer()
    {
        leftJoystickInput *= MovementSpeed;
        Player.GetComponent<Rigidbody2D>().velocity = leftJoystickInput;
    }
        void InputManager()
    {
        leftJoystickInput = leftJoystick.GetInputDirection();
        rightJoystickInput = rightJoystick.GetInputDirection();
    }
    public IEnumerator BecomeInvincable()
    {
        Invincible = true;
        yield return new WaitForSecondsRealtime(InvincabilityTime);
        Invincible = false;
    }
}
