﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using JetBrains.Annotations;
using System.Net;

public class EnemyControls : MonoBehaviour
{
    [HideInInspector]
    public enum Orientation { BasicSlime, FireSlime, ProjectileSlime, ArmoredSlime, DergonBoss }
    [Header("   Enemy type")]
    [Space(10)]

    public Orientation EnemyType;
    public Enemies[] EnemyData;

    [Header("   Detection")]
    [Space(10)]

    public GameObject Player;
    public CircleCollider2D ViewRange;
    public BoxCollider2D Hitbox;
    public Rigidbody2D rb;

    [Header("   Enemy Specific Fields")]
    [Space(10)]

    public GameObject FirePoint;
    public Vector3 FirePointPosition;
    public Vector2 InterpolationPoints1;
    public Vector2 InterpolationPoints2;
    public float fraction = 0;
    public float Distance;
    public float RandomRangeOffset;
    [SerializeField]
    bool CanShoot;

    // Basic Gameobject correction code
    void Start()
    {
        if(Player == null)
        {
            Debug.LogError(this.gameObject.name + " Player not assigned");
            Player = GameObject.Find("Player");
            Debug.Log("Found Player");
        }
        if(rb == null)
        {
            rb = this.gameObject.GetComponent<Rigidbody2D>();
        }
    }
    
    // DETECT ENEMY TYPE CODE
    void Update()
    {
        if (EnemyType == Orientation.BasicSlime)
        {
            ViewRange.radius = EnemyData[0].ViewRange;
            this.gameObject.GetComponent<SpriteRenderer>().sprite = EnemyData[0].EnemyIcon;
            Hitbox.size = new Vector2(EnemyData[0].HitBoxSizeX, EnemyData[0].HitBoxSizeY);
            Hitbox.offset = new Vector2(EnemyData[0].HitBoxOffsetX, EnemyData[0].HitBoxOffsetY);
        }
        if (EnemyType == Orientation.FireSlime)
        {
            ViewRange.radius = EnemyData[1].ViewRange;
            this.gameObject.GetComponent<SpriteRenderer>().sprite = EnemyData[1].EnemyIcon;
            Hitbox.size = new Vector2(EnemyData[1].HitBoxSizeX, EnemyData[1].HitBoxSizeY);
            Hitbox.offset = new Vector2(EnemyData[1].HitBoxOffsetX, EnemyData[1].HitBoxOffsetY);
        }
        if (EnemyType == Orientation.ProjectileSlime)
        {
            ViewRange.radius = EnemyData[2].ViewRange;
            this.gameObject.GetComponent<SpriteRenderer>().sprite = EnemyData[2].EnemyIcon;
            Hitbox.size = new Vector2(EnemyData[2].HitBoxSizeX, EnemyData[2].HitBoxSizeY);
            Hitbox.offset = new Vector2(EnemyData[2].HitBoxOffsetX, EnemyData[2].HitBoxOffsetY);
        }
        if (EnemyType == Orientation.ArmoredSlime)
        {
            ViewRange.radius = EnemyData[3].ViewRange;
            this.gameObject.GetComponent<SpriteRenderer>().sprite = EnemyData[3].EnemyIcon;
            Hitbox.size = new Vector2(EnemyData[3].HitBoxSizeX, EnemyData[3].HitBoxSizeY);
            Hitbox.offset = new Vector2(EnemyData[3].HitBoxOffsetX, EnemyData[3].HitBoxOffsetY);
        }
        if(EnemyType == Orientation.DergonBoss)
        {
            ViewRange.radius = EnemyData[4].ViewRange;
            this.gameObject.GetComponent<SpriteRenderer>().sprite = EnemyData[4].EnemyIcon;
            Hitbox.size = new Vector2(EnemyData[4].HitBoxSizeX, EnemyData[4].HitBoxSizeY);
            Hitbox.offset = new Vector2(EnemyData[4].HitBoxOffsetX, EnemyData[4].HitBoxOffsetY);
        }
        float dist = Vector2.Distance(this.gameObject.transform.position, Player.transform.position);
        Distance = dist;
    }




    //Special Movement;
    private void FixedUpdate()
    {
        if (EnemyType == Orientation.DergonBoss)
        {
            transform.position = Vector3.Lerp(InterpolationPoints1, InterpolationPoints2, Mathf.PingPong(Time.time * EnemyData[4].MovementSpeed, 1.0f));
        }

    }

    // DETECTION CODE
    void OnTriggerStay2D(Collider2D collision)
    {
        if (collision.CompareTag("Player"))
        {
            if(EnemyType == Orientation.BasicSlime)
            {
                this.gameObject.transform.right = Player.transform.position - this.gameObject.transform.position;
                this.gameObject.transform.Translate(Vector2.right * EnemyData[0].MovementSpeed);
            }
            if(EnemyType == Orientation.FireSlime)
            {
                this.gameObject.transform.right = Player.transform.position - this.gameObject.transform.position;
                this.gameObject.transform.Translate(Vector2.right * EnemyData[1].MovementSpeed);
            }
            if(EnemyType == Orientation.ProjectileSlime)
            {
                this.gameObject.transform.right = Player.transform.position - this.gameObject.transform.position;
                if (Distance <= EnemyData[2].minDistance)
                {   return; }
                this.gameObject.transform.Translate(Vector2.right * EnemyData[2].MovementSpeed);
            }
            if (EnemyType == Orientation.ArmoredSlime)
            {
                this.gameObject.transform.right = Player.transform.position - this.gameObject.transform.position;
                this.gameObject.transform.Translate(Vector2.right * EnemyData[3].MovementSpeed);
            }
            if(EnemyType == Orientation.DergonBoss)
            {
                FirePoint.transform.right = Player.transform.position - FirePoint.transform.position;
                if(!CanShoot)
                {   return; }
                CanShoot = false;
                ShootProjectile(4);
            }
        }
    }

    // ATTACKING CODE

    void ShootProjectile(int EnemyDataNumber)
    {
        float ShotPercision = UnityEngine.Random.Range(0, 100); 
        if(ShotPercision <= EnemyData[EnemyDataNumber].Accuracy)
        {
            RandomRangeOffset = UnityEngine.Random.Range(-5, 5);
            while (RandomRangeOffset == 0)
            {
                RandomRangeOffset = UnityEngine.Random.Range(-5, 5);
            }
        }
        //Instantiate(EnemyData[EnemyDataNumber].Projectile, this.gameObject.transform.position, new Vector3(FirePoint.transform.rotation.x,  FirePoint.transform.rotation.y, RandomRangeOffset += FirePoint.transform.rotation.z));
        WaitPerShot(EnemyDataNumber);
    }
    IEnumerator WaitPerShot(int EnemyDataNumber)
    {
        CanShoot = true;
        yield return new WaitForSecondsRealtime(EnemyData[EnemyDataNumber].TimePerShot);
    }
    private void OnCollisionStay2D(Collision2D collision)
    {
        if(collision.collider.CompareTag("Player"))
        {
            if(Player.GetComponent<Player_Controller>().Invincible)
            {   return; }
            if (EnemyType == Orientation.BasicSlime)
            {
                Player.gameObject.GetComponent<Player_Controller>().health -= EnemyData[0].DamagePerHit;
                Player.GetComponent<Rigidbody2D>().AddForce(this.gameObject.transform.right * EnemyData[0].Knockback);
                StartCoroutine(Player.gameObject.GetComponent<Player_Controller>().BecomeInvincable());
            }
            if (EnemyType == Orientation.FireSlime)
            {
                Player.gameObject.GetComponent<Player_Controller>().health -= EnemyData[1].DamagePerHit;
                Player.GetComponent<Rigidbody2D>().AddForce(this.gameObject.transform.right * EnemyData[1].Knockback);
                StartCoroutine(Player.gameObject.GetComponent<Player_Controller>().BecomeInvincable());
            }
            if (EnemyType == Orientation.ProjectileSlime)
            {
                Player.gameObject.GetComponent<Player_Controller>().health -= EnemyData[2].DamagePerHit;
                Player.GetComponent<Rigidbody2D>().AddForce(this.gameObject.transform.right * EnemyData[2].Knockback);
                StartCoroutine(Player.gameObject.GetComponent<Player_Controller>().BecomeInvincable());
            }
            if (EnemyType == Orientation.ArmoredSlime)
            {
                Player.gameObject.GetComponent<Player_Controller>().health -= EnemyData[3].DamagePerHit;
                Player.GetComponent<Rigidbody2D>().AddForce(this.gameObject.transform.right * EnemyData[3].Knockback);
                StartCoroutine(Player.gameObject.GetComponent<Player_Controller>().BecomeInvincable());
            }
            if(EnemyType == Orientation.DergonBoss)
            {
                Player.gameObject.GetComponent<Player_Controller>().health -= EnemyData[4].DamagePerHit;
                Player.GetComponent<Rigidbody2D>().AddForce(-Player.gameObject.transform.right * EnemyData[4].Knockback);
                StartCoroutine(Player.gameObject.GetComponent<Player_Controller>().BecomeInvincable());
            }
        }
    }
}
