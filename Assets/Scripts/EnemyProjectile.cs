﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyProjectile : MonoBehaviour
{
    public enum ProjectileTypeAI { Straight_Forward, Chaser, Raycast }
    public ProjectileTypeAI Ai_type;
    public enum ProjectileType { Fireball, Arrow}
    public ProjectileData Data;
    // Start is called before the first frame update
    void Start()
    {
        
    }
    private void OnCollisionEnter2D(Collision2D collision)
    {
        Player_Controller player = collision.collider.gameObject.GetComponent<Player_Controller>();
        if (player != null)
        {
            player.health += -Data.Damage;
        }
        Destroy(this.gameObject);
    }
    // Update is called once per frame
    void Update()
    {
        if (Ai_type == ProjectileTypeAI.Straight_Forward)
        {
            this.gameObject.transform.Translate(Vector2.right * Data.Speed);
        }
    }
}
