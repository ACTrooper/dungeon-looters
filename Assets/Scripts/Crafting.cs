﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Crafting : MonoBehaviour
{
    public GameObject CraftingUI;
   void OnTriggerEnter2D(Collider2D other) 
   {
       if(other.CompareTag("Player"))
       {
           CraftingUI.SetActive(true);
       }
   }
   void OnTriggerExit2D(Collider2D other) 
   {
       if(other.CompareTag("Player"))
       {
           CraftingUI.SetActive(false);
       }
   }
}
