﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class PowerUp : MonoBehaviour
{
    [Serializable]
    public class VariableHolder
    {
        public bool ManaStar;
        public bool HealthHeart;
        public bool GoldHealthOrb;
        public bool SpeedIncrease;
        public bool EXPORB;
    }
 
    public VariableHolder PowerUpType = new VariableHolder();
    public float manaIncreasePerPickup;
    Rigidbody2D rigidbody;
    public float Forceamount;
    public int healthIncreasePerPickup;
    public int MaxhealthIncreasePerPickup;
    public float expIncreasePerPickup;
    public float SpeedIncreaseperPickup;
    public GameObject[] DeathParticals;
    // Start is called before the first frame update
    void Start()
    {
        rigidbody = this.GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void Update()
    {

    }
    void OnTriggerEnter2D(Collider2D other)
    {
            Debug.Log("Collision With Player");
            if(PowerUpType.ManaStar)
            { ManaStarUp(other); }
            else if(PowerUpType.HealthHeart)
            { HealthUp(other); }
            else if(PowerUpType.EXPORB)
            { expUP(other); }
            else if(PowerUpType.SpeedIncrease)
            { SpeedUp(other); }
            else if(PowerUpType.GoldHealthOrb)
            { ExtraHeart(other); }
            else
            {
                Debug.LogError("Not valid string for power up type");
            }
    }
    void ExtraHeart(Collider2D player)
    {
        Debug.Log("Added an extra heart of Health");
        player.gameObject.GetComponent<Player_Controller>().MaxHealth += MaxhealthIncreasePerPickup;
        Instantiate(DeathParticals[0], this.gameObject.transform.position, new Quaternion(0,0,0,0));
        Destroy(this.gameObject);
    }
    void SpeedUp(Collider2D player)
    {
        Debug.Log("Added an extra heart of Health");
        player.gameObject.GetComponent<Player_Controller>().MovementSpeed += SpeedIncreaseperPickup;
        Instantiate(DeathParticals[0], this.gameObject.transform.position, new Quaternion(0,0,0,0));
        Destroy(this.gameObject);
    }
    void ManaStarUp(Collider2D player)
    {
        Debug.Log("Added Mana");
        player.gameObject.GetComponent<Player_Controller>().mana += manaIncreasePerPickup;
        Instantiate(DeathParticals[1], this.gameObject.transform.position, new Quaternion(0,0,0,0));
        Destroy(this.gameObject);
    }
    void HealthUp(Collider2D player)
    {
        Debug.Log("Added Health");
        player.gameObject.GetComponent<Player_Controller>().health += healthIncreasePerPickup;
        Instantiate(DeathParticals[0], this.gameObject.transform.position, new Quaternion(0,0,0,0));
        Destroy(this.gameObject);
    }
    void expUP(Collider2D player)
    {
        Debug.Log("Added EXP");
        player.gameObject.GetComponent<Player_Controller>().exp += expIncreasePerPickup;
        Destroy(this.gameObject);
    }
}
