﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Projectile", menuName = "Custom Assets/Projectile")]
public class ProjectileData : ScriptableObject
{
    public string Name;
    public GameObject ProjectileGameObject;
    public float Speed;
    public int Damage;
}
