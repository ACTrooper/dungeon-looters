﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Weapon", menuName = "Custom Assets/Weapon")]
public class Weapons : ScriptableObject
{
    public string Gunname;
    public string Description;
    public float firerate;
    public float Damage;

}
