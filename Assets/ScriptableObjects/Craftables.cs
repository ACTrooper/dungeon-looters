﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UIElements;

[CreateAssetMenu(menuName = "Custom Assets/ItemData")]
public class Craftables : ScriptableObject
{
    public string ItemName;
    public string description;
    public int costWood;
    public int costMetal;
    public int costGold;
    public float expPerCreation;
    public Image ItemSprite;
}
