﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Custom Assets/EnemyData")]
public class Enemies : ScriptableObject
{
    [Header("Defines")]
    [Space(10)]

    public string EnemyName;
    public string ClassName;
    public string Description;
    public Sprite EnemyIcon;

    [Header("Attacking")]
    [Space(10)]

    public int Health;
    public int Defense;
    public int DamagePerHit;
    public float MovementSpeed;
    public float Knockback;
    public float KnockbackTaken;

    [Header("Detection")]
    [Space(10)]

    public float ViewRange;
    public float HitBoxSizeX;
    public float HitBoxSizeY;
    public float HitBoxOffsetX;
    public float HitBoxOffsetY;

    [Header("Specific")]
    [Space(10)]

    public string EffectName;
    public float EffectLastTime;

    [Header("Projectile")]
    [Space(10)]

    public GameObject Projectile;
    public float TimePerShot;
    public float minDistance;
    public float Accuracy;
}
